var gulp = require('gulp'),
  less = require('gulp-less'),
  jade = require('gulp-jade'),
  browserSync = require('browser-sync'),
  changed = require('gulp-changed'),
  concat = require('gulp-concat'),
  reload = browserSync.reload;

gulp.task('less', function () {
  return gulp.src('./assets/less/**/*.less')
    .pipe(less())
    .pipe(gulp.dest('./public/assets/css/'))
    .pipe(reload({stream: true}));
});

gulp.task('jade2html', function () {
  gulp.src('./*.jade')
    .pipe(jade({pretty: true}))
    .pipe(gulp.dest('./public/'))
    .pipe(reload({stream: true}));
});

gulp.task('browser-sync', function () {
  browserSync({
    server: {
      baseDir: "./public",
      routes: {
        '/bower_components': './bower_components'
      }
    }
  });
});

gulp.task('copy:images', function () {
  return gulp.src('./assets/images/**/*')
    .pipe(changed('./public/assets/images/'))
    .pipe(gulp.dest('./public/assets/images/'))
    .pipe(reload({stream: true}));
});

gulp.task('copy:fonts', function () {
  return gulp.src(['./assets/fonts/*', './bower_components/bootstrap/fonts/*', './bower_components/font-awesome/fonts/*'])
    .pipe(gulp.dest('./public/assets/fonts/'))
});

gulp.task('copy:js', function () {
  return gulp.src('./assets/js/*')
    .pipe(gulp.dest('./public/assets/js/'));
});

gulp.task('concatCSS', function () {
  return gulp.src(['./bower_components/bootstrap/dist/css/bootstrap.css', 'bower_components/font-awesome/css/font-awesome.css', 'bower_components/flexslider/flexslider.css','bower_components/idilia-owl-carousel/owl-carousel/owl.carousel.css', 'bower_components/idilia-owl-carousel/owl-carousel/owl.theme.css', 'bower_components/sweetalert/dist/sweetalert.css'])
    .pipe(concat('assets.css'))
    .pipe(gulp.dest('./public/assets/css'));
});

gulp.task('concatJS', function () {
  return gulp.src(['./bower_components/jquery/dist/jquery.js', './bower_components/bootstrap/dist/js/bootstrap.js', 'bower_components/flexslider/jquery.flexslider.js', 'bower_components/idilia-owl-carousel/owl-carousel/owl.carousel.js', 'bower_components/sweetalert/dist/sweetalert.min.js', 'bower_components/jQuery-viewport-checker/dist/jquery.viewportchecker.min.js', 'bower_components/jquery-animateNumber/jquery.animateNumber.min.js'])
    .pipe(concat('assets.js'))
    .pipe(gulp.dest('./public/assets/js'));
});

gulp.task('default', ['copy:js', 'copy:images', 'copy:fonts', 'less', 'concatCSS', 'concatJS', 'jade2html', 'browser-sync'], function () {
  gulp.watch("./assets/js/*.js", ['copy:js']);
  gulp.watch("./public/assets/js/*.js", ['concatJS']);
  gulp.watch('./assets/images/**/*', ['copy:images']);
  gulp.watch("./assets/less/**/*.less", ['less']);
  gulp.watch("./public/assets/css/*.css", ['concatCSS']);
  gulp.watch("./*.jade", ['jade2html']);
});
